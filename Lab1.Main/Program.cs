﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
   

    public class Program
    {
        public IList<IMalpa> Kolekcja()
        {
            Goryl goryl = new Goryl();
            Pawian pawian = new Pawian();

            List<IMalpa> Lista = new List<IMalpa>() { goryl, pawian };

            return Lista;
        }

        public void Testing(IList<IMalpa> lista)
        {

            foreach (var i in lista)
            {
                i.Funkcja();
            }

        }

        
        static void Main(string[] args)
        {
            
           

        }
    }
}
