﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;
using Lab1.Main;

namespace Lab1.Implementation
{
    class Goryl : IGoryl
    {
        public Goryl() { }

        public string Funkcja()
        {
            return "test";
        }

        public string ZjedzBanana()
        {
            return "om nom nom";
        }

        string IGoryl.ZjedzBanana()
        {
            return "om nom nom";
        }

        string IMalpa.Funkcja()
        {
            return "test";
        }
    }
}
