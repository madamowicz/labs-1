﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(Lab1.Contract.IMalpa);
        
        public static Type ISub1 = typeof(Lab1.Contract.IGoryl);
        public static Type Impl1 = typeof(Lab1.Implementation.Goryl);
        
        public static Type ISub2 = typeof(Lab1.Contract.IPawian);
        public static Type Impl2 = typeof(Lab1.Implementation.Pawian);
        
        
        public static string baseMethod = "Funkcja";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "ZjedzBanana";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "IdzSpac";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "Kolekcja";
        public static string collectionConsumerMethod = "Testing";
        
        #endregion

        #region P3

        public static Type IOther = typeof(Lab1.Contract.IOlbrzym);
        public static Type Impl3 = typeof(Lab1.Implementation.Olbrzym);

        public static string otherCommonMethod = "Funkcja";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
