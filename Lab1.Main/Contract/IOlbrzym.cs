﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Contract
{
    public interface IOlbrzym : IMalpa
    {
        string NapijSie();
        string Funkcja();
    }
}
